import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:timeline_tile/timeline_tile.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final int currentHoure = DateTime.now().day;
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _scrollController.jumpTo((currentHoure - 1) * 100.0);
    });

    BorderRadiusGeometry radius = const BorderRadius.only(
      topLeft: Radius.circular(24.0),
      topRight: Radius.circular(24.0),
    );
    return Scaffold(
      body: Column(
        children: [
          SlidingUpPanel(
            borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(50.0),
                bottomRight: Radius.circular(50.0)),
            maxHeight: 200,
            minHeight: 300,
            slideDirection: SlideDirection.DOWN,
            panel: Padding(
              padding: const EdgeInsets.only(top: 90),
              child: Column(
                children: const [
                  Center(
                    child: Text("MODE EN PRÉSENTIEL"),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 28.0),
                    child: Icon(
                      Icons.arrow_downward_rounded,
                      size: 40,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 70,
            child: ListView.builder(
                controller: _scrollController,
                scrollDirection: Axis.horizontal,
                itemCount: hours.length,
                itemBuilder: (BuildContext context, int index) {
                  return TimelineTile(
                    axis: TimelineAxis.horizontal,
                    alignment: TimelineAlign.center,
                    isFirst: index == 0,
                    isLast: index == hours.length - 1,
                    beforeLineStyle:
                        LineStyle(color: Colors.black38.withOpacity(0.8)),
                    indicatorStyle: IndicatorStyle(
                      color: index == currentHoure - 1
                          ? Colors.purpleAccent
                          : Colors.black,
                      indicator: Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: index == currentHoure - 1
                                  ? const Color(0xFF35577D)
                                  : Colors.grey,
                              blurRadius: 20,
                              spreadRadius: 1,
                            ),
                          ],
                          shape: BoxShape.circle,
                          color: index == currentHoure - 1
                              ? const Color(0xFF35577D)
                              : Colors.grey,
                        ),
                      ),
                    ),
                    endChild: Container(
                      child: Center(
                        child: Text(
                          hours[index],
                          style: TextStyle(
                            fontSize: 18,
                            color: index == currentHoure - 1
                                ? const Color(0xFF35577D)
                                : Colors.black26,
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
      extendBody: true,
      bottomNavigationBar: FloatingNavbar(
        onTap: (int val) {
          //returns tab id which is user tapped
        },
        currentIndex: 0,
        items: [
          FloatingNavbarItem(icon: Icons.video_library, title: 'Cours'),
          FloatingNavbarItem(icon: Icons.fact_check_outlined, title: 'Tests'),
          FloatingNavbarItem(icon: Icons.chat_bubble_outline, title: 'Message'),
          FloatingNavbarItem(icon: Icons.settings, title: 'Paramètres'),
        ],
      ),
    );
  }
}

const hours = [
  '8H ',
  '9H',
  '10H',
  '11H',
  '12H',
  '13H',
  '14H',
  '15H',
  '16H',
  '17H',
  '18H'
];
