import 'package:flutter/material.dart';

import 'home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/FirstConnexion': (context) => Home(),
      },
      initialRoute: '/FirstConnexion',
      debugShowCheckedModeBanner: false,
    );
  }
}
